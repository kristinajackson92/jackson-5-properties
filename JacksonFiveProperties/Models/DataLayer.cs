﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Data;

namespace JacksonFiveProperties.Models
{
    public class DataLayer
    {
        SqlConnection Con;
        string s_Con;
        public DataLayer()
        {
            Connectin();
        }
        private void Connectin()
        {
            s_Con = ConfigurationManager.ConnectionStrings["Db"].ToString();
            Con = new SqlConnection(s_Con);
            if (Con.State == System.Data.ConnectionState.Open)
            {
                Con.Close();
            }
            Con.Open();
        }
        public List<ImageModel> GetFileList()
        {
            List<ImageModel> DetList = new List<ImageModel>();
            DetList = SqlMapper.Query<ImageModel>(Con, "GetFileDetails", commandType: CommandType.StoredProcedure).ToList();
            return DetList;
        }
        public bool SaveFileDetails(ImageModel objDet)
        {
            try
            {
                DynamicParameters Parm = new DynamicParameters();
                Parm.Add("@FileName", objDet.FileName);
                Parm.Add("@FileContent", objDet.FileContent);
                Con.Execute("AddFileDetails", Parm, commandType: System.Data.CommandType.StoredProcedure);
                return true;
            }
            catch (Exception E) { return false; }
        }
    }
}