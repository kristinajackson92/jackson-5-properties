﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace JacksonFiveProperties.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.Landlord> Landlords { get; set; }

        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.Property> Properties { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.PropLandLrd> PropLandLrds  { get; set; }

        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.Tenant> Tenants { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.TentPropLnd> TentPropLnds { get; set; }

        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.Transactions> Transactions { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.Maintence> Maintences { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.DataFiles> DataFiles { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.FileModel> FileModels { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.LetterFiles> LetterFiles { get; set; }
        public System.Data.Entity.DbSet<JacksonFiveProperties.Models.LetterModel> LetterModels { get; set; }
    }
}