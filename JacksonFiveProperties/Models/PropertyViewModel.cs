﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JacksonFiveProperties.Models
{
    public class Landlord
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "First Name")]
        [Required]
        public string Firstname { get; set; }
        [Display(Name = "Last Name")]
        [Required]
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int Zip { get; set; }

        public DateTime dateModified { get; set; } = System.DateTime.Now;
        public bool IsUsed { get; set; }

        [Display(Name = "Full Name")]
        public string fullname
        {
            get { return Lastname + ", " + Firstname; }
        }

        public static List<SelectListItem> StatesList = new List<SelectListItem>()
    {
        new SelectListItem() {Text="Alabama", Value="AL"},
        new SelectListItem() { Text="Alaska", Value="AK"},
        new SelectListItem() { Text="Arizona", Value="AZ"},
        new SelectListItem() { Text="Arkansas", Value="AR"},
        new SelectListItem() { Text="California", Value="CA"},
        new SelectListItem() { Text="Colorado", Value="CO"},
        new SelectListItem() { Text="Connecticut", Value="CT"},
        new SelectListItem() { Text="District of Columbia", Value="DC"},
        new SelectListItem() { Text="Delaware", Value="DE"},
        new SelectListItem() { Text="Florida", Value="FL"},
        new SelectListItem() { Text="Georgia", Value="GA"},
        new SelectListItem() { Text="Hawaii", Value="HI"},
        new SelectListItem() { Text="Idaho", Value="ID"},
        new SelectListItem() { Text="Illinois", Value="IL"},
        new SelectListItem() { Text="Indiana", Value="IN"},
        new SelectListItem() { Text="Iowa", Value="IA"},
        new SelectListItem() { Text="Kansas", Value="KS"},
        new SelectListItem() { Text="Kentucky", Value="KY"},
        new SelectListItem() { Text="Louisiana", Value="LA"},
        new SelectListItem() { Text="Maine", Value="ME"},
        new SelectListItem() { Text="Maryland", Value="MD"},
        new SelectListItem() { Text="Massachusetts", Value="MA"},
        new SelectListItem() { Text="Michigan", Value="MI"},
        new SelectListItem() { Text="Minnesota", Value="MN"},
        new SelectListItem() { Text="Mississippi", Value="MS"},
        new SelectListItem() { Text="Missouri", Value="MO"},
        new SelectListItem() { Text="Montana", Value="MT"},
        new SelectListItem() { Text="Nebraska", Value="NE"},
        new SelectListItem() { Text="Nevada", Value="NV"},
        new SelectListItem() { Text="New Hampshire", Value="NH"},
        new SelectListItem() { Text="New Jersey", Value="NJ"},
        new SelectListItem() { Text="New Mexico", Value="NM"},
        new SelectListItem() { Text="New York", Value="NY"},
        new SelectListItem() { Text="North Carolina", Value="NC"},
        new SelectListItem() { Text="North Dakota", Value="ND"},
        new SelectListItem() { Text="Ohio", Value="OH"},
        new SelectListItem() { Text="Oklahoma", Value="OK"},
        new SelectListItem() { Text="Oregon", Value="OR"},
        new SelectListItem() { Text="Pennsylvania", Value="PA"},
        new SelectListItem() { Text="Rhode Island", Value="RI"},
        new SelectListItem() { Text="South Carolina", Value="SC"},
        new SelectListItem() { Text="South Dakota", Value="SD"},
        new SelectListItem() { Text="Tennessee", Value="TN", Selected = true},
        new SelectListItem() { Text="Texas", Value="TX"},
        new SelectListItem() { Text="Utah", Value="UT"},
        new SelectListItem() { Text="Vermont", Value="VT"},
        new SelectListItem() { Text="Virginia", Value="VA"},
        new SelectListItem() { Text="Washington", Value="WA"},
        new SelectListItem() { Text="West Virginia", Value="WV"},
        new SelectListItem() { Text="Wisconsin", Value="WI"},
        new SelectListItem() { Text="Wyoming", Value="WY"}
    };

    }

    public class PropLandLrd
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [Display(Name = "Owner")]
        public int LandlordID { get; set; }
        [Required]
        [Display(Name = "Property")]
        public int PropertyID { get; set; }
        
    }

    public class Property
    {
        [Key]
        public int ID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        [DataType(DataType.PostalCode)]
        public int Zip { get; set; }
        public int LandlordID { get; set; }
        public bool IsUsed { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
    }

    public class PropViewModel
    {
        public Property Property { get; set; }
        public PropLandLrd PropLandLrd { get; set; }        
        public SelectList owner { get; set; }
        public Landlord Landlord { get; set; }

    }

    public class TentPropLnd
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [Display(Name = "Owner")]
        public int LandownerID { get; set; }
        [Required]
        [Display(Name = "Property")]
        public int PropertyID { get; set; }
        [Required]
        public int TenantID { get; set; }
    }

    public class Tenant
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string Firstname { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string Lastname { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string Phonenumnber { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
        public bool IsUsed { get; set; }

        [Display(Name = "Full Name")]
        public string fullname
        {
            get { return Lastname + ", " + Firstname; }
        }
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Occupation { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "DOB")]
        public string dob { get; set; }
     //   public string MyProperty { get; set; }
    }

    public class TenantViewModel
    {
        public Tenant Tenant { get; set; }
        public TentPropLnd TentPropLnd { get; set; }
        public SelectList ProductTypes { get; set; }
        public SelectList Prop { get; set; }
        public  Property   Property { get; set; }

    }
    public class Maintence
    {
        [Key]
        public int ID { get; set; }
        //[Required]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        [Display(Name = "Date of Repair")]
        public DateTime Date { get; set; }
       // [Required]
        [Display(Name = "Address")]
        public int PropertyID { get; set; }
        [Display(Name = "Time of Repair")]
        public string Time { get; set; }
        [Display(Name = "Repaired By")]
        public string By { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description of Repair")]
        public string Description { get; set; }
        [Display(Name = "Parts Purchased")]
        public string Parts { get; set; }
        [Display(Name = "Personal Remarks")]
        public string LaborHours { get; set; }
        [Display(Name = "Total Price of Repair")]
        public string Amount { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
        public bool isUsed { get; set; }
        public string addss { get; set; }
    }
    public class Transactions
    {
        [Key]
        public int ID { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        [Display(Name = "Date Rent Recieved")]
        public DateTime Date { get; set; }
        [Required]
        [Display(Name = "Address")]
        public int PropertyID { get; set; }
        public string Type { get; set; }
        [Display(Name = "Rent Receipt No")]
        public string Reference { get; set; }
        [Display(Name ="Personal Remarks")]
        public string Description { get; set; }
        [Display(Name = "Cash In/Cash out")]
        //[Required]
        public string Debit { get; set; }
        [Display(Name = "Cash In")]
        public string Credit { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Rent Amount")]
        public string Amount { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
        public bool IsUsed { get; set; }

        public static List<SelectListItem> DebitList = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Cash Out", Value = "1" },
            new SelectListItem() { Text = "Cash In", Value = "2" },
        };
            
    }

    public class MaintenceViewModel
    {
        public Maintence Maintence { get; set; }
        public Property Property { get; set; }
        public SelectList Propertyselect { get; set; }
    }


    public class TransactionViewModel
    {
        public Transactions Transactions { get; set; }
        public Property Property { get; set; }
        public SelectList Propertyselect { get; set; }
    }

    public class ReportInfo
    {
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string ReportDescription { get; set; }
        public string ReportURL { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string ReportSummary { get; set; }
    }

    public class ImageModel
    {
        public int ID { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public HttpPostedFileBase Files { get; set; }
        public int PropertyID { get; set; }
        public int MaintenceID { get; set; }
    }
    public class FileModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
        public int MaintenceID { get; set; }
    }
    public class DataFiles
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
        public int MaintenceID { get; set; }
        public bool isUsed { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
    }

    public class LetterModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }
    public class LetterFiles
    {
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
        public bool isUsed { get; set; }
        public DateTime dateModified { get; set; } = System.DateTime.Now;
    }
}