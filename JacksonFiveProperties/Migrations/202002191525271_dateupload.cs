namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateupload : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataFiles", "dateModified", c => c.DateTime(nullable: false));
            AddColumn("dbo.LetterFiles", "dateModified", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LetterFiles", "dateModified");
            DropColumn("dbo.DataFiles", "dateModified");
        }
    }
}
