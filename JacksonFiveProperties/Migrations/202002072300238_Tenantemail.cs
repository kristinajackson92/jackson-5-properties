namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tenantemail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tenants", "Email", c => c.String());
            AddColumn("dbo.Tenants", "Occupation", c => c.String());
            AddColumn("dbo.Tenants", "dob", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tenants", "dob");
            DropColumn("dbo.Tenants", "Occupation");
            DropColumn("dbo.Tenants", "Email");
        }
    }
}
