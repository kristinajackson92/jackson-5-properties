namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLandlordModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Landlords", "dateModified", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Landlords", "dateModified");
        }
    }
}
