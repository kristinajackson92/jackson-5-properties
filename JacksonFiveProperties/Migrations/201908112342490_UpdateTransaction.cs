namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTransaction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        PropertyID = c.Int(nullable: false),
                        Type = c.String(),
                        Reference = c.String(),
                        Description = c.String(),
                        Debit = c.Int(nullable: false),
                        Credit = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        dateModified = c.DateTime(nullable: false),
                        IsUsed = c.Boolean(nullable: false),
                        Propertyselect_DataGroupField = c.String(),
                        Propertyselect_DataTextField = c.String(),
                        Propertyselect_DataValueField = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Transactions");
        }
    }
}
