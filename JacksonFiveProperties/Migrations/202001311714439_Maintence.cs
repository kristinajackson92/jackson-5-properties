namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Maintence : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Maintences",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        PropertyID = c.Int(nullable: false),
                        Time = c.String(),
                        By = c.String(),
                        Description = c.String(),
                        Parts = c.String(),
                        LaborHours = c.String(),
                        Amount = c.Int(nullable: false),
                        dateModified = c.DateTime(nullable: false),
                        isUsed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AlterColumn("dbo.Transactions", "Debit", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Debit", c => c.String(nullable: false));
            DropTable("dbo.Maintences");
        }
    }
}
