namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databyte : DbMigration
    {
        public override void Up()
        {
           Sql(" ALTER TABLE dbo.DataFiles DROP COLUMN Data;");
            Sql("ALTER TABLE dbo.DataFiles ADD Data varbinary(max); ");
        }
        
        public override void Down()
        {
           
        }
    }
}
