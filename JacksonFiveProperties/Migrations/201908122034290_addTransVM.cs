namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTransVM : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Transactions", "Propertyselect_DataGroupField");
            DropColumn("dbo.Transactions", "Propertyselect_DataTextField");
            DropColumn("dbo.Transactions", "Propertyselect_DataValueField");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "Propertyselect_DataValueField", c => c.String());
            AddColumn("dbo.Transactions", "Propertyselect_DataTextField", c => c.String());
            AddColumn("dbo.Transactions", "Propertyselect_DataGroupField", c => c.String());
        }
    }
}
