namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changetotaltostring : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Maintences", "Amount", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Maintences", "Amount", c => c.Int(nullable: false));
        }
    }
}
