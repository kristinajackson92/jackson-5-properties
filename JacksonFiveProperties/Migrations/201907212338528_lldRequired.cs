namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lldRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Landlords", "Firstname", c => c.String(nullable: false));
            AlterColumn("dbo.Landlords", "Lastname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Landlords", "Lastname", c => c.String());
            AlterColumn("dbo.Landlords", "Firstname", c => c.String());
        }
    }
}
