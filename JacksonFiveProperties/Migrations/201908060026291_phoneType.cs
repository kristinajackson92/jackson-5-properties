namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class phoneType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tenants", "Phonenumnber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tenants", "Phonenumnber", c => c.Int(nullable: false));
        }
    }
}
