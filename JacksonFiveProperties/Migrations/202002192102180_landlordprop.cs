namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class landlordprop : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Properties", "LandlordID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Properties", "LandlordID");
        }
    }
}
