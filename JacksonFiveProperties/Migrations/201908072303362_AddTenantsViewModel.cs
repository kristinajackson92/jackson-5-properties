namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTenantsViewModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TentPropLnds", "LandownerID", c => c.Int(nullable: false));
            DropColumn("dbo.TentPropLnds", "PropLandLrdID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TentPropLnds", "PropLandLrdID", c => c.Int(nullable: false));
            DropColumn("dbo.TentPropLnds", "LandownerID");
        }
    }
}
