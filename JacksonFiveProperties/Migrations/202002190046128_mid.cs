namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileModels", "MaintenceID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileModels", "MaintenceID");
        }
    }
}
