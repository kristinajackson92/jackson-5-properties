namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetypemaitnence : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Maintences", "Date", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Maintences", "Date", c => c.DateTime(nullable: false));
        }
    }
}
