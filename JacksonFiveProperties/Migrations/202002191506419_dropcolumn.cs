namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropcolumn : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.LetterFiles", "Description");
            DropColumn("dbo.LetterModels", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LetterModels", "Description", c => c.String());
            AddColumn("dbo.LetterFiles", "Description", c => c.String());
        }
    }
}
