namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tenantPrpLnd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TentPropLnds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PropLandLrdID = c.Int(nullable: false),
                        PropertyID = c.Int(nullable: false),
                        TenantID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TentPropLnds");
        }
    }
}
