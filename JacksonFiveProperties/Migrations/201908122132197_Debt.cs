namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Debt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "Debit", c => c.String(nullable: false));
            AlterColumn("dbo.Transactions", "Credit", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Credit", c => c.Int(nullable: false));
            AlterColumn("dbo.Transactions", "Debit", c => c.Int(nullable: false));
        }
    }
}
