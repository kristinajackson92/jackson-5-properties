namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcolumnstoDataFiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataFiles", "MaintenceID", c => c.Int(nullable: false));
            AddColumn("dbo.DataFiles", "isUsed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataFiles", "isUsed");
            DropColumn("dbo.DataFiles", "MaintenceID");
        }
    }
}
