namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePropLand : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PropLandLrds",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LandlordID = c.Int(nullable: false),
                        PropertyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PropLandLrds");
        }
    }
}
