namespace JacksonFiveProperties.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _byte : DbMigration
    {
        public override void Up()
        {
            Sql("Truncate table dbo.DataFiles");
        //    AlterColumn("dbo.DataFiles", "Data", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DataFiles", "Data", c => c.String());
        }
    }
}
