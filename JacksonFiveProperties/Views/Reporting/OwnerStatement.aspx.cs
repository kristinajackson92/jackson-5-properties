﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JacksonFiveProperties.Views.Reporting
{
    public partial class OwnerStatement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    String reportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsFolder"].ToString();

                    ReportViewer1.Height = Unit.Pixel(Convert.ToInt32(Request["Height"]) - 58);
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

                    ReportViewer1.ServerReport.ReportServerUrl = new Uri("SSRS URL"); // Add the Reporting Server URL  
                    ReportViewer1.ServerReport.ReportPath = String.Format("/{0}/{1}", reportFolder, Request["ReportName"].ToString());

                    ReportViewer1.ServerReport.Refresh();
                }
                catch (Exception ex)
                {

                }
            }
        }
}
    }