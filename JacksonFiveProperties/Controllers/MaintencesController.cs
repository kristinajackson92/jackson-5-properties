﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using PagedList;

namespace JacksonFiveProperties.Controllers
{
    [Authorize]
    public class MaintencesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Maintences
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            List<Maintence> TransVMlist = new List<Maintence>(); // to hold list of Customer and order details

            var query = (from trans in db.Maintences
                         join prop in db.Properties on trans.PropertyID equals prop.ID
                         where trans.isUsed == true
                         select new
                         {
                             Maintence = trans,
                             Property = prop,
                             address = (prop.Address)
                         });
            if (!String.IsNullOrEmpty(searchString))
            {
                query = (from trans in db.Maintences
                         join prop in db.Properties on trans.PropertyID equals prop.ID
                         where trans.isUsed == true
                         && trans.addss.Contains(searchString)
                         select new
                         {
                             Maintence = trans,
                             Property = prop,
                             address = (prop.Address)
                         });
            }
            switch (sortOrder)
            {
                case "name_desc":
                    query = query.OrderByDescending(s => s.address);
                    break;
                case "Date":
                    query = query.OrderBy(s => s.Maintence.Date);
                    break;
                case "date_desc":
                    query = query.OrderByDescending(s => s.Maintence.Date);
                    break;
                default:
                    query = query.OrderBy(s => s.address);
                    break;
            }
            foreach (var item in query)

            {

                Maintence objcvm = new Maintence(); // ViewModel
                objcvm.Amount = item.Maintence.Amount;
                objcvm.Time = item.Maintence.Time;
                objcvm.dateModified = item.Maintence.dateModified;
                objcvm.By = item.Maintence.By;
                objcvm.Description = item.Maintence.Description;
                objcvm.ID = item.Maintence.ID;
                objcvm.isUsed = item.Maintence.isUsed;
                objcvm.PropertyID = item.Maintence.PropertyID;
                objcvm.Parts = item.Maintence.Parts;
                objcvm.LaborHours = item.Maintence.LaborHours;
                objcvm.Date = item.Maintence.Date;
                objcvm.addss = item.address;

                TransVMlist.Add(objcvm);

            }
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(TransVMlist.ToPagedList(pageNumber, pageSize));
        }

        // GET: Maintences/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maintence maintence = await db.Maintences.FindAsync(id);
            if (maintence == null)
            {
                return HttpNotFound();
            }
            return View(maintence);
        }

        // GET: Maintences/Create
        public ActionResult Create()
        {
            ViewBag.data = db.Properties.ToList();
            return View();
        }

        // POST: Maintences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Date,PropertyID,Time,By,Description,Parts,LaborHours,Amount,dateModified,isUsed")] Maintence maintence)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                    var property = Request["property"];
                    maintence.PropertyID = Convert.ToInt32(property);
                    maintence.isUsed = true;
                    db.Maintences.Add(maintence);
                    await db.SaveChangesAsync();
                    TempData["Success"] = "Saved Successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }


            return View(maintence);
        }

        // GET: Maintences/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maintence maintence = await db.Maintences.FindAsync(id);
            if (maintence == null)
            {
                return HttpNotFound();
            }
            var model = new MaintenceViewModel()
            {
                Maintence = db.Maintences.Where(x => x.isUsed == true).FirstOrDefault(s => s.ID == id),
                
            };

            model.Propertyselect = new SelectList(db.Properties.Where(x => x.IsUsed == true).ToList(), "ID", "Address", maintence.PropertyID);
            return View(model);
        }

        // POST: Maintences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(MaintenceViewModel vmmaintence)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                    
                    var maintnece = db.Maintences.FirstOrDefault(n => n.ID == vmmaintence.Maintence.ID);

                    maintnece.isUsed = true;
                    maintnece.Time = vmmaintence.Maintence.Time;
                    maintnece.By = vmmaintence.Maintence.By;
                    maintnece.Description = vmmaintence.Maintence.Description;
                    maintnece.Parts = vmmaintence.Maintence.Parts;
                    maintnece.Date = vmmaintence.Maintence.Date;
                    maintnece.Amount = vmmaintence.Maintence.Amount;
                    maintnece.LaborHours = vmmaintence.Maintence.LaborHours;
                    maintnece.PropertyID = vmmaintence.Maintence.PropertyID;
                    //db.Entry(maintnece).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    TempData["Success"] = "Saved Successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
            return View(vmmaintence);
        }

        // GET: Maintences/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maintence maintence = await db.Maintences.FindAsync(id);
            if (maintence == null)
            {
                return HttpNotFound();
            }
            return View(maintence);
        }

        // POST: Maintences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                Maintence maintence = await db.Maintences.FindAsync(id);
                maintence.isUsed = false;
                //db.Maintences.Remove(maintence);
                await db.SaveChangesAsync();
                TempData["Success"] = "Deleted Successfully!";
                return RedirectToAction("Index");
            }
            catch 
                (Exception ex)
            {
                    TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                    ModelState.AddModelError("Error!", ex.Message.ToString());
                }
            return View("Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpGet]
        public ActionResult UploadFile(int id)
        {
            //return View();
            TempData["mid"] = id;
            return View(GetFiles(id));
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase postedFile)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                byte[] bytes;
                int id;
                using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                {
                    bytes = br.ReadBytes(postedFile.ContentLength);
                    id = Convert.ToInt32(TempData["mid"]);
                }
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    string query = "INSERT INTO DataFiles VALUES (@Name, @ContentType, @Data, @isUsed, @MaintenceID)";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Name", Path.GetFileName(postedFile.FileName));
                        cmd.Parameters.AddWithValue("@ContentType", postedFile.ContentType);
                        cmd.Parameters.AddWithValue("@Data", bytes);
                        cmd.Parameters.AddWithValue("@isUsed", 1);
                        cmd.Parameters.AddWithValue("@MaintenceID", id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

                return View(GetFiles(id));
            }


            //}
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());

                return View("Error");
            }
           
        }
        [HttpPost]
        public FileResult DownloadFile(int? fileId)
        {
                byte[] bytes;
                string fileName, contentType;
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SELECT Name, Data, ContentType FROM DataFiles WHERE ID=@Id AND isUsed = 1 AND MaintenceID = @mid";
                        cmd.Parameters.AddWithValue("@Id", fileId);
                        cmd.Connection = con;
                        con.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            sdr.Read();
                            bytes = (byte[])sdr["Data"];
                            contentType = sdr["ContentType"].ToString();
                            fileName = sdr["Name"].ToString();
                        }
                        con.Close();
                    }
                }

                return File(bytes, contentType, fileName);
        }

    
        private static List<FileModel> GetFiles(int mid)
        {
            List<FileModel> files = new List<FileModel>();
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                    
                {
                    cmd.CommandText = "SELECT ID, Name,MaintenceID FROM DataFiles where MaintenceID = @mid AND isUsed = 1";
                    cmd.Parameters.AddWithValue("mid", mid);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            files.Add(new FileModel
                            {
                                Id = Convert.ToInt32(sdr["Id"]),
                                Name = sdr["Name"].ToString(),
                                MaintenceID = Convert.ToInt32(sdr["MaintenceID"])
                            });
                        }
                    }
                    con.Close();
                }
            }
            return files;
        }

        [HttpGet]
        public ActionResult DeleteFile(int id, int mid)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    string query = "UPDATE DataFiles SET isUsed = 0 WHERE Id = @id ";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Id", id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                TempData["Success"] = "Deleted Successfully!";
                return RedirectToAction("UploadFile", new { id = mid });
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());

                return View("Error");
            }

            //  return View(GetFiles(mid));
            // return Json(Url.Action("UploadFiles", "Maintences", new { id = id}));
            // return View(id);
        }
    }
}
