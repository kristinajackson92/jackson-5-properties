﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;

namespace JacksonFiveProperties.Views
{
    [Authorize]
    public class LandlordsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Landlords
        public async Task<ActionResult> Index()
        {

            return View(await db.Landlords.Where(x => x.IsUsed.Equals(true)).ToListAsync());
        }

        // GET: Landlords/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Landlord landlord = await db.Landlords.FindAsync(id);
            if (landlord == null)
            {
                return HttpNotFound();
            }
            return View(landlord);
        }

        // GET: Landlords/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Landlords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Firstname,Lastname,Address,City,State,Zip")] Landlord landlord)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    landlord.IsUsed = true;
                    db.Landlords.Add(landlord);
                    await db.SaveChangesAsync();
                    TempData["Success"] = "Landowner saved successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
            return View(landlord);
        }

        // GET: Landlords/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                //ModelState.AddModelError("Email", "Email not found or matched");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Landlord landlord = await db.Landlords.FindAsync(id);
            if (landlord == null)
            {
                return HttpNotFound();
            }
            return View(landlord);
        }

        // POST: Landlords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Firstname,Lastname,Address,City,State,Zip")] Landlord landlord)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    landlord.IsUsed = true;
                    db.Entry(landlord).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    TempData["Success"] = "Landowner updated successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
                return View(landlord);
        }

        // GET: Landlords/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Landlord landlord = await db.Landlords.FindAsync(id);
            if (landlord == null)
            {
                return HttpNotFound();
            }
            return View(landlord);
        }

        // POST: Landlords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {

                Landlord landlord = await db.Landlords.FindAsync(id);
                landlord.IsUsed = false;
                //db.Landlords.Remove(landlord);
                db.Entry(landlord).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["Success"] = "Landowner deleted successfully!";
                return RedirectToAction("Index");
            }
            catch 
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                return View("Error");

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
