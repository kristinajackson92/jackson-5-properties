﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;
using PagedList;

namespace JacksonFiveProperties.Controllers
{
    [Authorize]
    public class PropertiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Properties
        public ActionResult Index1(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var propertyindex = db.Properties.Where(x => x.IsUsed.Equals(true));
            if (!String.IsNullOrEmpty(searchString))
            {
                propertyindex = db.Properties.Where(x => x.Address.Contains(searchString) && x.IsUsed.Equals(true));
            }

                switch (sortOrder)
            {
                case "name_desc":
                    propertyindex = propertyindex.OrderByDescending(s => s.Address.ToString());
                    break;
                default:
                    propertyindex = propertyindex.OrderBy(s => s.Address);
                    break;
            }
            //  return View(db.Properties.Where(x => x.IsUsed.Equals(true)).ToList());
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(propertyindex.ToPagedList(pageNumber, pageSize));
        }

        // GET: Properties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        // GET: Properties/Create
        public ActionResult Create1()
        {
            var lldID = db.Landlords.ToList();
            if (lldID != null)
            {
                ViewBag.data = lldID;
            }
            return View();
        }

        // POST: Properties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create1([Bind(Include = "ID,Address,City,State,Zip")] Property property, string Datas)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                    var value = Request["fullname"];
                    property.IsUsed = true;                    
                    db.Properties.Add(property);
                    db.SaveChanges();
                    PropLandLrd propLand = new PropLandLrd();
                    
                    property.LandlordID = Convert.ToInt32(value);
                    propLand.PropertyID = Convert.ToInt32(property.ID);
                    db.PropLandLrds.Add(propLand);
                    db.SaveChanges();
                    TempData["Success"] = "Property saved successfully!";
                    return RedirectToAction("Index1");

                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
            return View(property);
        }

        // GET: Properties/Edit/5
        public ActionResult Edit1(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }

            var model = new PropViewModel()
            {
                Property =  db.Properties.Where(x => x.IsUsed == true).FirstOrDefault(s => s.ID == id),
                PropLandLrd = db.PropLandLrds.FirstOrDefault(s => s.PropertyID == id),
            };
            model.owner = new SelectList(db.Landlords.Where(x => x.IsUsed == true).ToList(), "ID", "fullname", model.PropLandLrd.LandlordID);
            ViewBag.owner = new SelectList(db.Landlords.Where(x => x.IsUsed == true).ToList(), "ID", "fullname", model.PropLandLrd.LandlordID);
            return View(model);
        }

        // POST: Properties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit1( PropViewModel propertymodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["Success"] = null;
                    TempData["Error"] = null;
                    var prop = db.Properties.FirstOrDefault(n => n.ID == propertymodel.Property.ID);
                    var propLrd = db.PropLandLrds.FirstOrDefault(n => n.PropertyID == propertymodel.Property.ID);

                    prop.Address = propertymodel.Property.Address;
                    prop.City = propertymodel.Property.City;
                    prop.IsUsed = true;
                    prop.State = propertymodel.Property.State;
                    prop.Zip = propertymodel.Property.Zip;
                    propLrd.LandlordID = Convert.ToInt32(propertymodel.PropLandLrd.LandlordID);

                    //db.Entry(property).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Success"] = "Property saved successfully!";
                    return RedirectToAction("Index1");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
            return View(propertymodel);
        }

        // GET: Properties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        // POST: Properties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                Property property = db.Properties.Find(id);
                property.IsUsed = false;
                db.Entry(property).State = EntityState.Modified;
                //db.Properties.Remove(property);
                db.SaveChanges();
                TempData["Success"] = "Property deleted successfully!";
                return RedirectToAction("Index1");
            }
            catch 
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                return View("Error");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
