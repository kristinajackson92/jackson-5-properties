﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using PagedList;

namespace JacksonFiveProperties.Controllers
{
    [Authorize]
    public class LetterFilesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: LetterFiles
        [HttpGet]
        public ActionResult Index()
        {
            //   return View(await db.LetterFiles.ToListAsync());
            return View(GetFiles());
        }

        private static List<LetterModel> GetFiles()
        {
            List<LetterModel> files = new List<LetterModel>();
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())

                {
                    cmd.CommandText = "SELECT ID, Name FROM LetterFiles where isUsed = 1";
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            files.Add(new LetterModel
                            {
                                Id = Convert.ToInt32(sdr["Id"]),
                                Name = sdr["Name"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }
            return files;
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase postedFile)
        {
            try
            {
                byte[] bytes;
                using (BinaryReader br = new BinaryReader(postedFile.InputStream))
                {
                    bytes = br.ReadBytes(postedFile.ContentLength);
                }
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    string query = "INSERT INTO LetterFiles VALUES (@Name, @ContentType, @Data, @isUsed)";
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Name", Path.GetFileName(postedFile.FileName));
                        cmd.Parameters.AddWithValue("@ContentType", postedFile.ContentType);
                        cmd.Parameters.AddWithValue("@Data", bytes);
                        cmd.Parameters.AddWithValue("@isUsed", 1);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

                return View(GetFiles());
            }

            catch (Exception ex)
            {
                ModelState.AddModelError("Error!", ex.Message.ToString());
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }
        [HttpPost]
        public FileResult DownloadFile(int? fileId)
        {

            byte[] bytes;
            string fileName, contentType;
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT Name, Data, ContentType FROM LetterFiles WHERE ID=@Id AND isUsed = 1";
                    cmd.Parameters.AddWithValue("@Id", fileId);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Data"];
                        contentType = sdr["ContentType"].ToString();
                        fileName = sdr["Name"].ToString();
                    }
                    con.Close();
                }
            }

            return File(bytes, contentType, fileName);
        }

        // GET: LetterFiles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LetterFiles letterFiles = await db.LetterFiles.FindAsync(id);
            if (letterFiles == null)
            {
                return HttpNotFound();
            }
            return View(letterFiles);
        }

        [HttpGet]
        public ActionResult DeleteFile(int id)
        {

            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                string query = "UPDATE LetterFiles SET isUsed = 0 WHERE Id = @id ";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@Id", id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            return RedirectToAction("Index");

            //  return View(GetFiles(mid));
            // return Json(Url.Action("UploadFiles", "Maintences", new { id = id}));
            // return View(id);
        }

        // GET: LetterFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LetterFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,ContentType,Data,Description,isUsed")] LetterFiles letterFiles)
        {
            if (ModelState.IsValid)
            {
                db.LetterFiles.Add(letterFiles);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(letterFiles);
        }

        // GET: LetterFiles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LetterFiles letterFiles = await db.LetterFiles.FindAsync(id);
            if (letterFiles == null)
            {
                return HttpNotFound();
            }
            return View(letterFiles);
        }

        // POST: LetterFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,ContentType,Data,Description,isUsed")] LetterFiles letterFiles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(letterFiles).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(letterFiles);
        }

        // GET: LetterFiles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LetterFiles letterFiles = await db.LetterFiles.FindAsync(id);
            if (letterFiles == null)
            {
                return HttpNotFound();
            }
            return View(letterFiles);
        }

        // POST: LetterFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            LetterFiles letterFiles = await db.LetterFiles.FindAsync(id);
            db.LetterFiles.Remove(letterFiles);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
