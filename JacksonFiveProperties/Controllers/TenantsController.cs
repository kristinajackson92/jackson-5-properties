﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace JacksonFiveProperties.Controllers
{
    [Authorize]
    public class TenantsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tenants
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var lldID = (from tl in db.TentPropLnds
                         join p in db.Properties
                         on tl.PropertyID equals p.ID
                         join t in db.Tenants on tl.TenantID equals t.ID
                         where t.IsUsed == true || p.IsUsed == true
                         select new TenantViewModel { Tenant = t, Property = p });

            if (!String.IsNullOrEmpty(searchString))
            {
                lldID = (from tl in db.TentPropLnds
                         join p in db.Properties
                         on tl.PropertyID equals p.ID
                         join t in db.Tenants on tl.TenantID equals t.ID
                         where (t.Lastname.Contains(searchString) && p.IsUsed == true )
                         || (t.Firstname.Contains(searchString) && p.IsUsed ==true )
                         select new TenantViewModel { Tenant = t, Property = p });
            }
            switch (sortOrder)
            {
                case "name_desc":
                    lldID = lldID.OrderByDescending(s => s.Tenant.Lastname);
                    break;
                case "Date":
                    lldID = lldID.OrderBy(s => s.Tenant.Firstname);
                    break;
                case "date_desc":
                    lldID = lldID.OrderByDescending(s => s.Tenant.Firstname);
                    break;
                default:
                    lldID = lldID.OrderBy(s => s.Tenant.Lastname);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(lldID.ToPagedList(pageNumber, pageSize));
            // return View(db.Tenants.Where(x => x.IsUsed.Equals(true)).ToList());
        }

        // GET: Tenants/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tenant tenant = db.Tenants.Find(id);
            if (tenant == null)
            {
                return HttpNotFound();
            }
            return View(tenant);
        }

        // GET: Tenants/Create
        public ActionResult Create()
        {
            var lldID = (from pl in db.PropLandLrds
                         join l in db.Landlords
                         on pl.LandlordID equals l.ID
                         join p in db.Properties
                         on pl.PropertyID equals p.ID
                         where l.IsUsed == true || p.IsUsed == true
                         select new { landlordid = l.ID, propertyid = p.ID, ownername = l.Lastname + " , " + l.Firstname, property = p.Address, id = pl.ID }).ToList();
            if (lldID != null)
            {
                ViewBag.data = lldID.DistinctBy(i => i.landlordid);
                ViewBag.Pdata = lldID;
            }
            return View();
        }

        // POST: Tenants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Firstname,Lastname,Phonenumnber,dob,Email,Occupation")] Tenant tenant)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {

                    var owner = Request["ownername"];
                    var property = Request["property"];
                    tenant.IsUsed = true;
                    db.Tenants.Add(tenant);
                    db.SaveChanges();

                    TentPropLnd propLand = new TentPropLnd();

                    propLand.LandownerID = Convert.ToInt32(owner);
                    propLand.PropertyID = Convert.ToInt32(property);
                    propLand.TenantID = Convert.ToInt32(tenant.ID);
                    db.TentPropLnds.Add(propLand);
                    db.SaveChanges();
                    TempData["Success"] = "Saved successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.InnerException.Message.ToString());
            }

            return View(tenant);
        }

        // GET: Tenants/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tenant tenant = db.Tenants.Find(id);
            if (tenant == null)
            {
                return HttpNotFound();
            }


            var model = new TenantViewModel()
            {
                Tenant = db.Tenants.Where(x => x.IsUsed == true).FirstOrDefault(s => s.ID == id),
                TentPropLnd = db.TentPropLnds.FirstOrDefault(s => s.TenantID == id),
        };
            model.ProductTypes = new SelectList(db.Landlords.Where(x => x.IsUsed == true).ToList(), "ID", "fullname", model.TentPropLnd.LandownerID);
            model.Prop = new SelectList(db.Properties.Where(x => x.IsUsed == true).ToList(), "ID", "Address", model.TentPropLnd.PropertyID);

            return View(model);
        }

        // POST: Tenants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TenantViewModel vmtenant)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                    var tenanat = db.Tenants.FirstOrDefault(n => n.ID == vmtenant.Tenant.ID);
                    var tentprop = db.TentPropLnds.FirstOrDefault(n => n.TenantID == vmtenant.Tenant.ID);

                        tenanat.Firstname = vmtenant.Tenant.Firstname;
                        tenanat.Lastname = vmtenant.Tenant.Lastname;
                        tenanat.Phonenumnber = vmtenant.Tenant.Phonenumnber;
                        tenanat.IsUsed = true;
                        tentprop.LandownerID = vmtenant.TentPropLnd.LandownerID;
                        tentprop.PropertyID = vmtenant.TentPropLnd.PropertyID;
                        tentprop.TenantID = vmtenant.Tenant.ID;
                    
                    //db.Entry(tenanat).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Success"] = "Saved successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.InnerException.Message.ToString());
            }
            return View(vmtenant);
        }

        // GET: Tenants/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tenant tenant = db.Tenants.Find(id);
            if (tenant == null)
            {
                return HttpNotFound();
            }
            return View(tenant);
        }

        // POST: Tenants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                Tenant tenant = db.Tenants.Find(id);
                tenant.IsUsed = false;
                //db.Tenants.Remove(tenant);
                db.Entry(tenant).State = EntityState.Modified;
                db.SaveChanges();
                TempData["Success"] = "Saved successfully!";
                return RedirectToAction("Index");
            }
            catch 
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                return View("Error");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
