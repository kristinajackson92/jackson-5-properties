﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JacksonFiveProperties.Models;
using PagedList;

namespace JacksonFiveProperties.Controllers
{
    [Authorize]
    public class TransactionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Transactions
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            List<Transactions> TransVMlist = new List<Transactions>(); // to hold list of Customer and order details

            var query = (from trans in db.Transactions
                         join prop in db.Properties on trans.PropertyID equals prop.ID
                         where trans.IsUsed == true
                         select new { Transactions = trans,
                             Property = prop,
                             Debits = (trans.Debit.Equals("1") ? "Cash Out" : 
                                        trans.Debit.Equals("0") ? "Cash In" : "")});
            if (!String.IsNullOrEmpty(searchString))
            {
                 query = (from trans in db.Transactions
                             join prop in db.Properties on trans.PropertyID equals prop.ID
                             where trans.IsUsed == true
                             && prop.Address.Contains(searchString)
                             select new
                             {
                                 Transactions = trans,
                                 Property = prop,
                                 Debits = (trans.Debit.Equals("1") ? "Cash Out" :
                                            trans.Debit.Equals("0") ? "Cash In" : "")
                             });
            }
            switch (sortOrder)
            {
                case "name_desc":
                    query = query.OrderByDescending(s => s.Property.Address);
                    break;
                case "Date":
                    query = query.OrderBy(s => s.Transactions.Date);
                    break;
                case "date_desc":
                    query = query.OrderByDescending(s => s.Transactions.Date);
                    break;
                default:
                    query = query.OrderBy(s => s.Property.Address);
                    break;
            }
            foreach (var item in query)

            {

                Transactions objcvm = new Transactions(); // ViewModel
                objcvm.Amount = item.Transactions.Amount;
                objcvm.Credit = item.Property.Address;
                objcvm.dateModified = item.Transactions.dateModified;
                objcvm.Debit = item.Debits;
                objcvm.Description = item.Transactions.Description;
                objcvm.ID = item.Transactions.ID;
                objcvm.IsUsed = item.Transactions.IsUsed;
                objcvm.PropertyID = item.Transactions.PropertyID;
                objcvm.Reference = item.Transactions.Reference;
                objcvm.Type = item.Transactions.Type;
                objcvm.Date = item.Transactions.Date;
                

                TransVMlist.Add(objcvm);

            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            //  return View(db.Transactions.Where(x => x.IsUsed.Equals(true)).ToList());
            return View(TransVMlist.ToPagedList(pageNumber, pageSize));
        }

        // GET: Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transactions transactions = db.Transactions.Find(id);
            if (transactions == null)
            {
                return HttpNotFound();
            }
            return View(transactions);
        }

        // GET: Transactions/Create
        public ActionResult Create()
        {
                ViewBag.data = db.Properties.ToList();

            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Date,PropertyID,Type,Reference,Description,Debit,Credit,Amount,dateModified,IsUsed")] Transactions transactions)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                   
                    var property = Request["property"];
                    transactions.PropertyID = Convert.ToInt32(property);
                    transactions.IsUsed = true;
                    db.Transactions.Add(transactions);
                    db.SaveChanges();
                    TempData["Success"] = "Saved Successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }

            return View(transactions);
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transactions transactions = db.Transactions.Find(id);
            if (transactions == null)
            {
                return HttpNotFound();
            }

            var model = new TransactionViewModel()
            {
                Transactions = db.Transactions.Where(x => x.IsUsed == true).FirstOrDefault(s => s.ID == id),
            };

            model.Propertyselect = new SelectList(db.Properties.Where(x => x.IsUsed == true).ToList(), "ID", "Address", transactions.PropertyID);
            return View(model);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TransactionViewModel vmtransaction)
        {
            try
            {
                TempData["Success"] = null;
                TempData["Error"] = null;
                if (ModelState.IsValid)
                {
                    var transactions = db.Transactions.FirstOrDefault(n => n.ID == vmtransaction.Transactions.ID);

                    transactions.IsUsed = true;
                    transactions.Reference = vmtransaction.Transactions.Reference;
                    transactions.Type = vmtransaction.Transactions.Type;
                    transactions.Description = vmtransaction.Transactions.Description;
                    transactions.Debit = vmtransaction.Transactions.Debit;
                    transactions.Date = vmtransaction.Transactions.Date;
                    transactions.Amount = vmtransaction.Transactions.Amount;
                    transactions.Credit = vmtransaction.Transactions.Credit;
                    transactions.PropertyID = vmtransaction.Transactions.PropertyID;

                    // db.Entry(transactions).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["Success"] = "Saved Successfully!";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Oops! Something Unexpected happend, data did not save!";
                ModelState.AddModelError("Error!", ex.Message.ToString());
            }
            return View(vmtransaction);
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transactions transactions = db.Transactions.Find(id);
            if (transactions == null)
            {
                return HttpNotFound();
            }
            return View(transactions);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TempData["Success"] = null;
            TempData["Error"] = null;
            Transactions transactions = db.Transactions.Find(id);
            transactions.IsUsed = false;
            //db.Transactions.Remove(transactions);
            db.Entry(transactions).State = EntityState.Modified;
            db.SaveChanges();
            TempData["Success"] = "Deleted successfully!";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
