﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JacksonFiveProperties.Startup))]
namespace JacksonFiveProperties
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
